# LIS4331 - Advanced Mobile Application Development

## Evan Pollak

## Assignment 5

### Assignment Requirements:
1. Create Android Application that displays RSS feed with link to article via Chrome
2. Include splash screen image with app title and list of articles
3. Include article title and description on item activity screen
4. Add background color(s) and other themeing elements
5. Create and display launcher icon image

### Assignment Screenshots:

Items acitivty | Item Activity | Read More...
- | - | -
![alt](img/1.png) | ![alt](img/2.png) | ![alt](img/3.png)