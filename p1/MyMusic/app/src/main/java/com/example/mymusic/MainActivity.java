package com.example.mymusic;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button button1, button2, button3;
    MediaPlayer mpMacMiller, mpSampha, mpBeatles;
    int playing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        button1 = (Button) findViewById(R.id.btnMacMiller);
        button2 = (Button) findViewById(R.id.btnSampha);
        button3 = (Button) findViewById(R.id.btnBeatles);
        button1.setOnClickListener(bMacMiller);
        button2.setOnClickListener(bSampha);
        button3.setOnClickListener(bBeatles);

        mpMacMiller = new MediaPlayer();
        mpMacMiller = MediaPlayer.create(this, R.raw.macmiller);

        mpSampha = new MediaPlayer();
        mpSampha = MediaPlayer.create(this, R.raw.sampha);

        mpBeatles = new MediaPlayer();
        mpBeatles = MediaPlayer.create(this, R.raw.beatles);

        playing = 0;
    }

    Button.OnClickListener bMacMiller = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch(playing){
                case 0:
                    mpMacMiller.start();
                    playing = 1;
                    button1.setText("Pause Mac Miller");
                    button2.setVisibility(View.INVISIBLE);
                    button3.setVisibility(View.INVISIBLE);
                    break;
                case 1:
                    mpMacMiller.pause();
                    playing = 0;
                    button1.setText("Play Mac Miller");
                    button2.setVisibility(View.VISIBLE);
                    button3.setVisibility(View.VISIBLE);
                    break;
            }
        }

    };

    Button.OnClickListener bSampha = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch(playing){
                case 0:
                    mpSampha.start();
                    playing = 1;
                    button2.setText("Pause Sampha");
                    button1.setVisibility(View.INVISIBLE);
                    button3.setVisibility(View.INVISIBLE);
                    break;
                case 1:
                    mpSampha.pause();
                    playing = 0;
                    button2.setText("Play Sampha");
                    button1.setVisibility(View.VISIBLE);
                    button3.setVisibility(View.VISIBLE);
                    break;
            }
        }

    };

    Button.OnClickListener bBeatles = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch(playing){
                case 0:
                    mpBeatles.start();
                    playing = 1;
                    button3.setText("Pause The Beatles");
                    button1.setVisibility(View.INVISIBLE);
                    button2.setVisibility(View.INVISIBLE);
                    break;
                case 1:
                    mpBeatles.pause();
                    playing = 0;
                    button3.setText("Play The Beatles");
                    button1.setVisibility(View.VISIBLE);
                    button2.setVisibility(View.VISIBLE);
                    break;
            }
        }

    };
}
