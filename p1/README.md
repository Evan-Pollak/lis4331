# LIS4331 - Advanced Mobile Application Development

## Evan Pollak

## Project 1

### Assignment Requirements:
1. Create Android Application that plays music
2. Create Splash screen thats displayed on application startup
3. Customize application by changing backgrounds, adding launcher icon, and changing text color
4. Add buttons that enable playing/pausing music in Java
5. Add media player objects for the songs

### Assignment Screenshots:

Splash Screen | Follow-up Screen
- | -
![alt](img/1.png) | ![alt](img/2.png)

Play/Pause Interface |
- | -
![alt](img/3.png) |
