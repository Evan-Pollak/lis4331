# LIS4331 - Advanced Mobile Application Development

## Evan Pollak

## Assignment 1

### Assignment Requirements:
1. Install JDK
2. Install Android Studio
3. Write and Compile Hello.java
4. Create first app in Android Studio
5. Create Contact app in Android Studio

### Git Commands:
1. git init - Create new repository or initilizate an existing one
2. git status - See what files need to be added from local or which ones have been deleted
3. git add - Add new/modified file from local repo
4. git commit - Creates snapshot of the repo with the new changes
5. git push - Upload local repo to remote one
6. git pull - Download remote repo content to local one
7. git clone - Create copy of existing repo

### Assignment Screenshots:

Hello.java | First App
- | -
![alt](img/java.png) | ![alt](img/first_app.png)

Contact App 1 | Contact App 2
- | -
![alt](img/1.png) | ![alt](img/2.png)


### Assignment Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Evan-Pollak/bitbucketstationlocations/src/master/)

*Assignment 1:*
[A1 Repo Link](https://bitbucket.org/Evan-Pollak/lis4331/src/master/a1/)

