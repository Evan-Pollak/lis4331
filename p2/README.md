# LIS4331 - Advanced Mobile Application Development

## Evan Pollak

## Project 2

### Assignment Requirements:
1. Create Android Application that acts as a task list for user
2. Include splash screen on application startup
3. Use SQLITE to store tasks
4. Add functionality to insert/update/delete tasks
5. Add background color(s) and other themeing elements
6. Create and display launcher icon image

### Assignment Screenshots:

Splash Screen | Task List Activity
- | -
![alt](img/1.png) | ![alt](img/2.png)