package com.example.tasklist;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        TimerTask task = new TimerTask(){

            @Override
            public void run(){
                Intent i = new Intent(SplashScreen.this, TaskList.class);
                startActivity(i);
                finish();

            }
        };
        Timer opening = new Timer();
        opening.schedule(task,5000);

    }
}