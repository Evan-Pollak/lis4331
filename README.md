# LIS4331 - Advanced Mobile Application Development

## Evan Pollak

## LIS4331 Requirements:

*Course Work Links*

1. [A1 README.md](https://bitbucket.org/Evan-Pollak/lis4331/src/master/a1/README.md)
    * Install JDK
    * Install Android Studio
    * Write and Compile Hello.java
    * Create first app in Android Studio
    * Create Contact app in Android Studio

2. [A2 README.md](https://bitbucket.org/Evan-Pollak/lis4331/src/master/a2/README.md)
    * Create Android Application that calculates an even share of a restaraunt bill
    * Create drop-down menus so users can select guests and percentage of bill as tip
    * Code spinners and buttons in Java to respon to user-input
    * Convert strings to ints and create math function in Java
    * Customize application by adding background, image border, and changing font colors

3. [A3 README.md](https://bitbucket.org/Evan-Pollak/lis4331/src/master/a3/README.md)
    * Create Android Application that converts US currency to others at an exchange rate
    * Create radio buttons for user to select desired currency
    * Code radio group and button in java to respond to user input
    * Horizontally and Vertically align radio group
    * Customize application by adding background, button border, and changing font colors

4. [P1 README.md](https://bitbucket.org/Evan-Pollak/lis4331/src/master/p1/README.md)
    * Create Android Application that plays music
    * Create Splash Screen thats displayed on application startup
    * Customize application by changing backgrounds, adding launcher icon, and changing text color
    * Adds buttons that enable playing/pausing music in Java
    * Add media player objects for the songs

5. [A4 README.md](https://bitbucket.org/Evan-Pollak/lis4331/src/master/a4/README.md)
    * Include splash screen image, app title, intro text
    * Include appropriate images
    * Must use persistent data: SharedPreferences
    * Widgets and images must be vertically and horizontally aligned
    * Must add background color(s) or theme
    * Create and display launcher icon image

6. [A5 README.md](https://bitbucket.org/Evan-Pollak/lis4331/src/master/a5/README.md)
    * Create Android Application that displays RSS feed with link to article via Chrome
    * Include splash screen image with app title and list of articles
    * Include article title and description on item activity screen
    * Add background color(s) and other themeing elements
    * Create and display launcher icon image

7. [P2 READMEmd](https://bitbucket.org/Evan-Pollak/lis4331/src/master/p2/README.md)
    * Create Android Application that acts as a task list for user
    * Include splash screen on application startup
    * Use SQLITE to store tasks
    * Add functionality to insert/update/delete tasks
    * Add background color(s) and other themeing elements
    * Create and display launcher icon image
