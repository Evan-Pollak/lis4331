package com.example.mortgageinterestcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    float floatPayment;
    int intYears;
    float floatPrincipal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        final EditText payment = (EditText)findViewById(R.id.txtPayment);
        final EditText years = (EditText)findViewById(R.id.txtYears);
        final EditText principal = (EditText)findViewById(R.id.txtPrincipal);
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        Button button = (Button)findViewById(R.id.btnCalc);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                floatPayment = Float.parseFloat(payment.getText().toString());
                intYears = Integer.parseInt(years.getText().toString());
                floatPrincipal = Float.parseFloat(principal.getText().toString());

                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putFloat("key1", floatPayment);
                editor.putInt("key2", intYears);
                editor.putFloat("key3", floatPrincipal);
                editor.commit();
                startActivity(new Intent(MainActivity.this, Payment.class));

            }
        });
    }
}
