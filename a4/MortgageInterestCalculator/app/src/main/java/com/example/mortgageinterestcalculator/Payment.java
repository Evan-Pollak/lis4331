package com.example.mortgageinterestcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;

public class Payment extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        TextView totalPayment = (TextView)findViewById(R.id.txtTotal);
        ImageView image = (ImageView)findViewById(R.id.imgYears);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        float floatPayment = sharedPref.getFloat("key1", 0);
        int intYears = sharedPref.getInt("key2", 0);
        float floatPrincipal = sharedPref.getFloat("key3", 0);

        float fullPayment;

        fullPayment = (floatPayment * (intYears * 12) - floatPrincipal);
        DecimalFormat currency = new DecimalFormat("$###,###.##");
        totalPayment.setText("Total paid: " + currency.format(fullPayment));
        if (intYears == 10)
        {
            image.setImageResource(R.drawable.one);
        }
        else if (intYears == 20)
        {
            image.setImageResource(R.drawable.twenty);
        }
        else if (intYears == 30)
        {
            image.setImageResource(R.drawable.thirty);
        }
        else
        {
            totalPayment.setText("Enter 10, 20, or 30 years");
            image.setImageResource(R.drawable.mortgage);
        }


    }
}
