# LIS4331 - Advanced Mobile Application Development

## Evan Pollak

## Assignment 4

### Assignment Requirements:
1. Include splash screen image, app title, intro text
2. Include appropriate images
3. Must use persistent data: SharedPreferences
4. Widgets and images must be vertically and horizontally aligned
5. Must add background color(s) or theme
6. Create and display launcher icon image

### Assignment Screenshots:

Splash Screen | Main Screen | Invalid Entry
- | - | -
![alt](img/1.png) | ![alt](img/2.png) | ![alt](img/3.png)

10 years | 20 years | 30 years
- | - | -
![alt](img/4.png) | ![alt](img/5.png) | ![alt](img/6.png)