import java.text.NumberFormat;
import java.util.Scanner;


public class SimpleInterestCalc
{
    static void tPrompt()
    {
        System.out.println("Program performs the following functions:");
        System.out.println("1. Calculates amount of money saved for a period of years, at a specified interest rate (i.e., yearly, non-compounded");
        System.out.println("2. Returned amount is formatted in U.S. currency, and rounded to two decimal places.");
        System.out.println("\n***Note:*** Program checks for non-numeric values, as well as only integer values for years.");
    }

    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        double principal;
        double rate;
        int time;
        double total;
        tPrompt();

        System.out.print("Current principal: $");
        while(!sc.hasNextDouble())
        {
            System.out.println("Not valid number!\n");
            sc.next();
            System.out.print("Please try again. Enter principal: $");
        }
        principal = sc.nextDouble();


        System.out.print("\nInterest Rate (per year): ");
        while(!sc.hasNextDouble())
        {
            System.out.println("Not valid number!\n");
            sc.next();
            System.out.print("Please try again. Enter interest rate: ");
        }
        rate = sc.nextDouble();

        System.out.print("\nTotal time (in years): ");
        while(!sc.hasNextInt())
        {
            System.out.println("Not valid integer!\n");
            sc.next();
            System.out.print("Please try again, Enter years: ");
        }
        time = sc.nextInt();

        double intRate = rate / 100;
        total = principal * (1 + intRate * time);
        
        NumberFormat currency = NumberFormat.getCurrencyInstance();
        System.out.println("\nYou have saved " + currency.format(total) + " in " + time + " years, at an interest rate of " + rate + "%\n");
    }
}