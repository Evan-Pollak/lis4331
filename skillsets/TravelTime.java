//Evan Pollak
//Skillset 10 - Travel Time

import java.util.Scanner;

public class TravelTime
{
  public static void main(String args[])
  {
    System.out.println("Program calculates approximate travel time\n");
    Scanner input = new Scanner(System.in);
    String choice = "";
    double miles = 0;
    double mph = 0;
    double time = 0;
    boolean isValidMiles = false;
    boolean isValidMPH = false;
    do
    {
      while(isValidMiles == false)
      {
        System.out.print("Please enter miles: ");
        if(input.hasNextDouble())
        {
          miles = input.nextDouble();
          isValidMiles = true;
        }
        else
        {
          System.out.println("Invalid double--miles must be a number.\n");
        }
        input.nextLine();
        
        if(isValidMiles == true && miles <= 0 || miles > 3000)
        {
          System.out.println("Miles must be greater than 0, and no more than 3000.\n");
          isValidMiles = false;
        }
        else if(isValidMiles == true && isValidMPH == false)
        {
          while(isValidMPH == false)
          {
            System.out.print("Please enter MPH: ");
            if(input.hasNextDouble())
            {
              mph = input.nextDouble();
              isValidMPH = true;
            }
            else
            {
              System.out.println("Invalid double--MPH must be a number.\n");
            }
            input.nextLine();
            if(isValidMPH = true && mph <= 0 || mph > 100)
            {
              System.out.println("MPH must be greater than 0, and no more than 100.\n");
              isValidMPH = false;
            }
          }
        }
        if(isValidMiles == true && isValidMPH == true)
        {
          System.out.println();

          time = miles/mph;
          int hours = (int) time;
          int minutes = (int) (time*60)%60;

           System.out.printf("\nEstimated travel time: %d hr(s) %d Minutes\n", hours, minutes);
        }
      }
      isValidMiles = false;
      isValidMPH = false;

      System.out.print("\nContinue? (y/n): ");
      choice = input.next();
    } while((choice == "y") || (choice == "Y"));
  }
}
   
