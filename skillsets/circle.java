//Evan Pollak
//LIS 4331 - Skillset 1
//January 21, 2020

import java.util.Scanner;
import java.lang.Math;

public class circle
{
    public static void main(String args[])
    {
        System.out.println("Non-OOP program calculates diameter, circumference, and circle area.");
        System.out.println("Must use Java's built in PI constant, printf(), and formatted to 2 decimal places.");
        System.out.println("Must *only* permit numeric entry.\n");

        double radius = 0;
        double diameter = 0;
        double circumference = 0;
        double area = 0;
        Scanner input = new Scanner(System.in);

        
        System.out.print("Enter circle radius: ");
        while (!input.hasNextDouble())
        {
            System.out.println("Not valid number!\n");
            input.next();
            System.out.print("Please try again. Enter circle radius: ");
        }
        radius = input.nextDouble();

        System.out.println();

        diameter = radius * 2;
        circumference = 2 * Math.PI * radius;
        area = Math.PI * Math.pow(radius, 2);

        System.out.printf("Circle diameter: %.2f\n", diameter);
        System.out.printf("Circumference: %.2f\n", circumference);
        System.out.printf("Area: %.2f\n", area);
    }
}