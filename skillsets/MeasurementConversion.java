//Evan Pollak
//2.13.20
//Skilslet 7 - Measurement Conversion

import java.text.DecimalFormat;
import java.util.Scanner;

public class MeasurementConversion
{
  public static void main(String args[])
  {
    System.out.println("Program converts inches to centimeters, meters, feet, yards, and miles.");
    System.out.println("***Notes***:");
    System.out.println("1) Use integer for inches (must validate integer input).");
    System.out.println("2) Use printf() function to print (format values per below output).");
    System.out.println("3) Create java \"constants\" for the following values:");
    System.out.println("\t INCHES_TO_CENTIMETER,\n\t INCHES_TO_METER,\n\t INCHES_TO_FOOT,\n\t INCHES_TO_YARD,\n\t FEET_TO_MILE\n");

    Scanner input = new Scanner(System.in);

    final double INCHES_TO_CENTIMETER = 2.54;
    final double INCHES_TO_METER = 0.0254;
    final double INCHES_TO_FEET = 12;
    final double INCHES_TO_YARD = 36;
    final double FEET_TO_MILE = 5280;
    int inches = 0;

    System.out.print("Please enter number of inches: ");
    while(!input.hasNextInt())
    {
      System.out.println("Not valid integer!\n");
      input.next();
      System.out.print("Please enter number of seconds: ");
    }

    inches = input.nextInt();

    System.out.printf("%,d inches(s) equals\n\n", inches);

    double centimeter = INCHES_TO_CENTIMETER * inches;
    double meter = INCHES_TO_METER *  inches;
    double feet = inches / INCHES_TO_FEET;
    double yard = inches / INCHES_TO_YARD;
    double mile = feet / FEET_TO_MILE;

    System.out.printf("%,f centimeter(s)\n", centimeter);
    System.out.printf("%,f meter(s)\n", meter);
    System.out.printf("%,f feet\n", feet);
    System.out.printf("%,f yard(s)\n", yard);
    System.out.printf("%,.8f mile(s)\n", mile);

  }
}

    
