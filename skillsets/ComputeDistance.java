//Evan Pollak
//Skillset 8 - Java GUI
//Feb.18.2020

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.lang.Math;

public class ComputeDistance implements ActionListener
{
  public static void main(String[] args)
  {
    ComputeDistance gui = new ComputeDistance();
  }

  private JFrame frame;
  private JTextField legAField;
  private JTextField legBField;
  private JLabel distanceLabel;
  private JButton computeButton;

  public ComputeDistance()
  {
    legAField = new JTextField(5);
    legBField = new JTextField(5);
    distanceLabel = new JLabel("Compute Distance Leg C.");
    computeButton = new JButton("Compute");
    computeButton.addActionListener(this);

    JPanel north = new JPanel(new GridLayout(2, 2));
    north.add(new JLabel("Leg A: "));
    north.add(legAField);
    north.add(new JLabel("Leg B: "));
    north.add(legBField);

    frame = new JFrame("Compute Distance");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setLayout(new BorderLayout());
    frame.add(north, BorderLayout.NORTH);
    frame.add(distanceLabel, BorderLayout.CENTER);
    frame.add(computeButton, BorderLayout.SOUTH);
    frame.pack();
    frame.setVisible(true);
  }

  public void actionPerformed(ActionEvent event)
  {
    String legAText = legAField.getText();
    double legA = Double.parseDouble(legAText);
    String legBText = legBField.getText();
    double legB = Double.parseDouble(legBText);

    double legC = Math.sqrt((legA * legA) + (legB * legB));

    distanceLabel.setText("Leg C: " + legC);
  }
}
