//Evan Pollak
//Skillset 10 
import java.util.Scanner;
public class TravelTimes
{
 public static void main(String args[])
   {
      System.out.println("Program calculates approximate travel time\n");

      // declare variable
      Scanner input = new Scanner (System.in);
      double miles;
      double mph;
      String choice = "";
    do{
     
    System.out.print("Please enter miles: ");
    
      while (!input.hasNextDouble())
    {
        System.out.println("Invalid double--miles must be a number\n");
        input.next();
        System.out.print("Please enter number of miles: ");
    }   
    miles=input.nextDouble();
        if (miles<0||miles>3000)
    {
        System.out.println("Miles must be greater than 0, and no more than 3000.\n");
        System.out.print("Please enter miles: ");
        miles=input.nextDouble();
    }  else { continue; }
    System.out.print("Please enter MPH: ");
    while (!input.hasNextDouble())
    {
        System.out.println("Invalid double--MPH must be a number\n");
        input.next();
        System.out.print("Please enter MPH: ");
    }   
    mph=input.nextDouble();
    if (mph<0||mph>100)
    {
        System.out.println("Miles must be greater than 0, and no more than 3000.\n");
        System.out.print("Please enter MPH: ");
        mph=input.nextDouble();
    }   else { continue; }
    double time = miles/mph;
    int hours = (int) time;
    int minutes = (int) (time*60)%60;

    System.out.println("\nEstimated travel time: "+hours+" hr(s) "+minutes+" Minutes");
    System.out.print("\nDo you want to continue? (y/n): ");
    choice=input.next();
        }while(choice.equalsIgnoreCase("Y"));
    }
}
