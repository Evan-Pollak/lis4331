public class Book extends Product
{
  private String author;

  public Book()
  {
    super();
    System.out.println("\nInside book default constructor.");
    author = "John Doe";
  }

  public Book(String c, String d, double p, String a)
  {
    super(c, d, p);
    System.out.println("\nInside book constructor with parameters.");
    author = a;
  }

  public void setAuthor(String author)
  {
    this.author = author;
  }

  public String getAuthor()
  {
    return author;
  }

  @Override
  public void print()
  {
    super.print();
    System.out.println(", Author: " + author);
  }
}
