
import java.util.Scanner;


public class ProductDemo
{
  public static void main(String[] args)
  {
    String code = "";
    String description = "";
    double price = 0.0;
    Scanner input = new Scanner(System.in);

    System.out.printf("\n/////Below are default constructor values://///\n");
    Product p1 = new Product();
    System.out.printf("\nCode = %s\n", p1.getCode());
    System.out.printf("Description = %s\n", p1.getDescription());
    System.out.printf("Price = %s\n", p1.getPrice());

    System.out.printf("\n/////Below are user-entered values://///\n");

    System.out.print("\nCode: ");
    code = input.nextLine();

    System.out.print("Description: ");
    description = input.nextLine();

    System.out.print("Price: ");
    price = input.nextDouble();

    Product p2 = new Product(code, description, price);
    System.out.printf("\nCode = %s\n", p2.getCode());
    System.out.printf("Description = %s\n", p2.getDescription());
    System.out.printf("Price = %s\n", p2.getPrice());

    System.out.printf("\n/////Below using setter methods to pass literal values, then print() method to display values://///");
    p2.setCode("xyz789");
    p2.setDescription("Test Widget");
    p2.setPrice(89.99);
    p2.print();
  }
}


