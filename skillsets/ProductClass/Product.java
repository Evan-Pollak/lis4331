
import java.text.NumberFormat;

public class Product
{
  private String code;
  private String description;
  private double price;
  public static int count = 0;

  public Product()
  {
    System.out.println("\nInside product default constructor.");
    code = "abc123";
    description = "My Widget";
    price = 49.99;
  }

  public Product(String code, String description, double price)
  {
    System.out.println("\nInside product constructor with parameters.");
    this.code = code;
    this.description = description;
    this.price = price;
  }

  public String getCode()
  {
    return code;
  }

  public void setCode(String c)
  {
    code = c;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String d)
  {
    description = d;
  }

  public String getPrice()
  {
    NumberFormat currency = NumberFormat.getCurrencyInstance();
    return currency.format(price);
  }

  public void setPrice(double p)
  {
    price = p;
  }

  public void print()
  {
    System.out.printf("\n\nCode: %s, Description: %s, Price: %s\n\n", code, description, getPrice());
  }
}

