//Evan Pollak
//Skillset 3 - Nested Structures 2
//1.28.20

import java.util.Scanner;

public class nestedstruct
{
  public static void main(String args[])
  {
    System.out.println("Program counts, totals, and averages total number of user-entered exam scores.");
    System.out.println("Please enter exam scores between 0 and 100, inclusive.");
    System.out.println("Enter out of range number to end program.");
    System.out.println("Must *only* permit numeric entry.\n");
    
    Scanner input = new Scanner(System.in);

    int num = 0;
    int sum = 0;
    int count = 0;
    int average;

    while(num <= 100 && num >= 0)
    {
      System.out.print("Enter exam score: ");
      while (!input.hasNextInt())
      {
        System.out.println("Not valid number!\n");
        input.next();
        System.out.print("Please try again. Enter exam score: ");
      }
      num = input.nextInt();
      if (num >= 0)
      {
         sum += num;
         count ++;
      }
      else
      {
        continue;
      }
    }
    
    System.out.println("Count: " + count);
    System.out.println("Total: " + sum);
    System.out.println("Average: " + sum / count);
  }
}

