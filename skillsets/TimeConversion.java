//Evan Pollak
//1.30.20
//Skillset 4 - Time Conversion

import java.text.DecimalFormat;
import java.util.Scanner;

public class TimeConversion
{
  public static void main(String args[])
  {
    System.out.println("Program converts seconds to minutes, hours, days, weeks, and (regular) years--365 days.");
    System.out.println("***Notes***:");
    System.out.println("1) Use integer for seconds (must validate integer input).");
    System.out.println("2) Use printf() function to print (format values per below output).");
    System.out.println("3) Create Java \"constants\" for the following values:");
    System.out.println("\t SECS_IN_MINS,\n\t MINS_IN_HR,\n\t HRS_IN_DAY,\n\t DAYS_IN_WEEK,\n\t DAYS_IN_YR (365 days)\n");
    
    Scanner input = new Scanner(System.in);

    final double SECS_IN_MINS = 60;
    final double  MINS_IN_HR = 60;
    final double HRS_IN_DAY = 24;
    final double DAYS_IN_WEEK = 7;
    final double  DAYS_IN_YR = 365;
    int seconds = 0; 
      
    System.out.print("Please enter number of seconds: ");
    while(!input.hasNextInt())
    {
      System.out.println("Not valid integer!\n");
      input.next();
      System.out.print("Please enter number of seconds: ");
    }
    seconds = input.nextInt();

    System.out.printf("%,d second(s) equals\n\n", seconds);
   
    double minutes = seconds / SECS_IN_MINS;
    double hours = minutes / MINS_IN_HR;
    double days = hours / HRS_IN_DAY;
    double weeks = days / DAYS_IN_WEEK;
    double years = days / DAYS_IN_YR;
   
    System.out.printf("%,.2f minute(s)\n", minutes);
    System.out.printf("%,.3f hour(s)\n", hours);
    System.out.printf("%,.4f day(s)\n", days);
    System.out.printf("%,.5f week(s)\n", weeks);
    System.out.printf("%,.6f year(s)\n\n", years);
  }
}
