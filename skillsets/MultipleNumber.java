//Evan Pollak
//Skillset 2
//1.23.20

import java.util.Scanner;
import java.lang.Math;

public class MultipleNumber
{
    public static void main(String args[])
    {
        System.out.printf("Program determines if first number is multiple of second, prints result.\n");
        System.out.printf("Example: 2, 4, 6, 8, are multiples of 2.\n");
        System.out.printf("1) Use integers. 2) Use printf() function to print.\n");
        System.out.printf("Must *only* permit integer entry.\n");

        int num1 = 0;
        int num2 = 0;

        Scanner input = new Scanner(System.in);

        System.out.printf("Num1: ");

        while(!input.hasNextInt())
        {
            System.out.printf("Not valid integer!\n");
            input.next();
            System.out.print("\nPlease try again. Enter Num1: ");
        }
        num1 = input.nextInt();

        System.out.println("Num2: ");
        while(!input.hasNextInt())
        {
            System.out.printf("Not valid integer!\n");
            input.next();
            System.out.printf("Please try again Enter Num2: \n");
        }
        num2 = input.nextInt();

        if(num1 % num2 == 0)
        {
            System.out.printf("\n%d is a multiple of %d\n", num1, num2);
            int val = num1 / num2;
            System.out.printf("The product of %d and %d is %d\n", val, num2, num1);
        }

        else
        {
            System.out.printf("\n%d is not a multiple of %d\n", num1, num2);
        }


    }
}

