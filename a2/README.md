# LIS4331 - Advanced Mobile Application Development

## Evan Pollak

## Assignment 2

### Assignment Requirements:
1. Create Android Application that calculates an even share of a restaraunt bill
2. Create drop-down menus so users can select guests and percentage of bill as tip
3. Code spinners and buttons in Java to respon to user-input
4. Convert strings to ints and create math function in Java
5. Customize application by adding background, image border, and changing font colors

### Assignment Screenshots:

Unpopulated | Populated
- | -
![alt](img/1.png) | ![alt](img/2.png)


