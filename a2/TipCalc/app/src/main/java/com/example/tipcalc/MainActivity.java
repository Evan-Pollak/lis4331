package com.example.tipcalc;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.util.Locale;


public class MainActivity extends AppCompatActivity {

    int numGuests = 0;
    double percentageTip = 0;
    int billCost = 0;
    double totalCost = 0.0;
    String tipPercentage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        final EditText total = (EditText)findViewById(R.id.txtBill);
        final Spinner guests = (Spinner)findViewById(R.id.txtGuests);
        final Spinner tipPercent = (Spinner)findViewById(R.id.txtPercent);
        Button cost = (Button)findViewById(R.id.btnCost);
        cost.setOnClickListener(new View.OnClickListener() {
            final TextView result = ((TextView)findViewById(R.id.txtResult));
            @Override
            public void onClick(View v) {
                billCost=Integer.parseInt(total.getText().toString());
                numGuests=Integer.parseInt(guests.getSelectedItem().toString());
                tipPercentage = (tipPercent.getSelectedItem().toString()).replace("%","");
                percentageTip = Integer.parseInt(tipPercentage);
                totalCost=((billCost * (percentageTip/100))+billCost) / numGuests;
                DecimalFormat currency = new DecimalFormat("$###,###.##");
                result.setText("Cost for each of " + numGuests + " guests: " + currency.format(totalCost));
            }
        });
    }
}
