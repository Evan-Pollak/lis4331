# LIS4331 - Advanced Mobile Application Development

## Evan Pollak

## Assignment 3

### Assignment Requirements:
1. Create Android Application that converts US currency to others at an exchange rate
2. Create radio buttons for user to select desired currency
3. Code radio group and button in java to respond to user input
4. Horizontally and Vertically align radio group
5. Customize application by adding background, button border, and changing font colors

### Assignment Screenshots:

Unpopulated | Populated
- | -
![alt](img/1.png) | ![alt](img/3.png)

Toast Notification | 
- | -
![alt](img/2.png) |