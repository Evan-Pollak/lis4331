package com.example.currencyconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import java.text.NumberFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    double convertedAmnt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        final EditText amnt = (EditText) findViewById(R.id.txtAmt);
        final RadioButton euRo = (RadioButton) findViewById(R.id.radioEuro);
        final RadioButton PeSo = (RadioButton) findViewById(R.id.radioPeso);
        final RadioButton CaNadian = (RadioButton) findViewById(R.id.radioCanada);
        final TextView result = ((TextView) findViewById(R.id.txtConverted));
        Button calculate = (Button) findViewById(R.id.btnCalc);
        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberFormat dollar = NumberFormat.getCurrencyInstance();
                Locale locale = new Locale("en","EU");
                NumberFormat EURO = NumberFormat.getCurrencyInstance(locale);
                double amntEntered = Double.parseDouble(amnt.getText().toString());
                if (amntEntered < 0 || amntEntered >= 100000)
                {
                    Toast.makeText(MainActivity.this, "US Dollars must be <= 100,000 and > 0", Toast.LENGTH_LONG).show();
                }
                else
                {
                    if(euRo.isChecked())
                    {
                        convertedAmnt = amntEntered * 0.92;
                        result.setText(EURO.format(convertedAmnt) + " Euros");
                    }

                    else if(PeSo.isChecked())
                    {
                        convertedAmnt = amntEntered * 18.60;
                        result.setText(dollar.format(convertedAmnt) + " Pesos");
                    }
                    else if(CaNadian.isChecked())
                    {
                        convertedAmnt = amntEntered * 1.32;
                        result.setText(dollar.format(convertedAmnt) + " CAD");
                    }
                }
            }
        });
    }

}
